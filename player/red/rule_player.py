from env.env_def import UnitType, RED_AIRPORT_ID, MapInfo
from common.cmd import Command
from common.grid import MapGrid
from common.interface.base_rule import BaseRulePlayer
from common.interface.task import Task, TaskState
import numpy as np

# 以下import内容为顾宇豪引入
from env.env_cmd import EnvCmd
from env.env_cmd import Point

A2G_TAKEOFF_AREA_HUNT_PARAMS = [270, 20000, 20000, 800]
AREA_PATROL_PARAMS = [270, 10000, 10000, 1000, 10800]
AWACS_AREA_PATROL_PARAMS = [270, 1, 1, 800, 10800, 0]
AWACS_LINE_PATROL_PARAMS = [800, 0, 'line']

RED_AIRPORT_POINT = [251004, 25526, 8000]
BLUE_SOUTH_COMMANDER_POINT = [-295519, -100815, 8000]
SOUTH_ASSEMBLY_POINT = [95154, -300000, 8000]
SOUTH_HUNT_POINT = [-260546, -202484, 8000]
NORTH_ASSEMBLY_POINT = [-6046, 205736, 8000]
AWACS_PATROL_POINT = [30564, -35864, 8000]

A2G_TEAM_SIZE = 2  # 单个轰炸机编队含轰炸机个数
A2A_TEAM_SIZE = 3  # 单个歼击机编队含歼击机个数


class Formation:
    def __init__(self, st):
        self.pt = []
        self.st = st


class RulePlayer(BaseRulePlayer):
    SOUTH_A2A_TEAM_NUM = 3
    SOUTH_A2G_TEAM_NUM = 4
    NORTH_A2A_TEAM_NUM = 4
    NORTH_A2G_TEAM_NUM = 5

    def __init__(self, side):
        super().__init__(side)
        self.agent_state = 0
        self.awacs_id = 0
        self.disturb_id = 0

    # 打印出当前已部署单位各自的数目
    def _get_units(self, raw_obs):
        unit_map = {}
        for unit in raw_obs[self.side]['units']:
            unit_map[unit['LX']] = unit_map.get(unit['LX'], 0) + 1
        print(unit_map)

    # 打印出已发现的敌方单位各自的数目
    def _get_en_units(self, raw_obs):
        unit_map = {}
        for unit in raw_obs[self.side]['qb']:
            unit_map[unit['LX']] = unit_map.get(unit['LX'], 0) + 1
        print(unit_map)

    def _a2g_attack(self, raw_obs, en_id):
        cmds = []
        for unit in raw_obs[self.side]['units']:
            if unit['LX'] == UnitType.A2G and unit['WP']['360'] > 0:  # and unit['WP']['360'] > 0
                cmds.append(EnvCmd.make_targethunt(unit['ID'], en_id, 270, 100))
                # break
        return cmds

    def step(self, raw_obs):
        cmds = []
        self.agent_state += 1
        if self.agent_state == 1:
            # 起飞南方编队
            for i in range(self.SOUTH_A2G_TEAM_NUM):
                cmds.append(
                    EnvCmd.make_takeoff_areapatrol(RED_AIRPORT_ID, A2G_TEAM_SIZE, UnitType.A2G,
                                                   *SOUTH_ASSEMBLY_POINT,
                                                   *AREA_PATROL_PARAMS))
            for i in range(self.SOUTH_A2A_TEAM_NUM):
                cmds.append(
                    EnvCmd.make_takeoff_areapatrol(RED_AIRPORT_ID, A2A_TEAM_SIZE, UnitType.A2A,
                                                   *SOUTH_ASSEMBLY_POINT,
                                                   *AREA_PATROL_PARAMS))
            # 起飞北方编队
            for i in range(self.NORTH_A2G_TEAM_NUM):
                cmds.append(
                    EnvCmd.make_takeoff_areapatrol(RED_AIRPORT_ID, A2G_TEAM_SIZE, UnitType.A2G,
                                                   *NORTH_ASSEMBLY_POINT,
                                                   *AREA_PATROL_PARAMS))
            for i in range(self.NORTH_A2A_TEAM_NUM):
                cmds.append(
                    EnvCmd.make_takeoff_areapatrol(RED_AIRPORT_ID, A2A_TEAM_SIZE, UnitType.A2A,
                                                   *NORTH_ASSEMBLY_POINT,
                                                   *AREA_PATROL_PARAMS))
            # 设置预警机
            for unit in raw_obs[self.side]['units']:
                if unit['LX'] == UnitType.AWACS:
                    self.awacs_id = unit['ID']
            cmds.append(
                EnvCmd.make_awcs_linepatrol(self.awacs_id, *AWACS_LINE_PATROL_PARAMS,
                                            [Point(*RED_AIRPORT_POINT), Point(*AWACS_PATROL_POINT)]))
            for i in range(1, 4):
                cmds.append(EnvCmd.make_takeoff_protect(RED_AIRPORT_ID, 1, self.awacs_id, i, 5, 1000))  # 歼击机护航预警机
        return cmds
